/*
 * Copyright 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.applaudostudios.codechallenge.android.viewmodel;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.applaudostudios.codechallenge.android.BR;
import com.applaudostudios.codechallenge.android.R;
import com.applaudostudios.codechallenge.android.model.Team;
import com.applaudostudios.codechallenge.android.utility.ApiManager;
import com.applaudostudios.codechallenge.android.utility.EndlessOnScrollListener;
import com.applaudostudios.codechallenge.android.view.adapters.TeamsAdapter;
import com.applaudostudios.codechallenge.android.view.fragments.TeamFragment;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Handle all the data between TeamFragment class
 * and fragment_team layout in order to update
 * all the data in the view.
 */
public class TeamViewModel extends BaseObservable{
    private int mListVisibility= View.INVISIBLE;
    private int mProgressBarVisibility= View.VISIBLE;
    private int mErrorMessageVisibility= View.INVISIBLE;
    private String mErrorMessage="";
    private static TeamsAdapter mTeamsAdapter;
    private static List<Team> mTeamList = new ArrayList<>();
    private Context mContext;
    private static TeamFragment.OnTeamSelectionListener mTeamListener;
    private static OnListBottomReachListener mOnListBottomReachListener;
    private static int mCurrentPage=1;
    private int mProgressBarListVisibility= View.GONE;

    /**
     * Interface to notify when the user reach the end of the
     * team list.
     */
     public interface OnListBottomReachListener{
         void onListBottomReach();
    }

    /**
     * Create a TeamViewModel instance to handle all the data
     * interaction between the TeamFragment and fragment team layout.
     * @param context application´s context
     * @param teamListener the interface to send data to TeamActivity
     */
    public TeamViewModel(Context context, TeamFragment.OnTeamSelectionListener teamListener,OnListBottomReachListener bottomReachListener){
        mContext=context;
        mTeamListener= teamListener;
        mOnListBottomReachListener=bottomReachListener;
    }

    @Bindable
    public int getListVisibility() {
        return mListVisibility;
    }

    public void setListVisibility(int listVisibility) {
        this.mListVisibility = listVisibility;
        notifyPropertyChanged(BR.listVisibility);
    }

    @Bindable
    public int getProgressBarVisibility() {
        return mProgressBarVisibility;
    }

    public void setProgressBarVisibility(int progressBarVisibility) {
        this.mProgressBarVisibility = progressBarVisibility;
        notifyPropertyChanged(BR.progressBarVisibility);
    }

    @Bindable
    public int getErrorMessageVisibility() {
        return mErrorMessageVisibility;
    }

    public void setErrorMessageVisibility(int errorMessageVisibility) {
        this.mErrorMessageVisibility = errorMessageVisibility;
        notifyPropertyChanged(BR.errorMessageVisibility);
    }

    @Bindable
    public String getErrorMessage() {
        return mErrorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.mErrorMessage = errorMessage;
        notifyPropertyChanged(BR.errorMessage);
    }

    @Bindable
    public List<Team> getTeamList() {
        return mTeamList;
    }

    public void setTeamList(List<Team> teamList) {
        this.mTeamList = teamList;
        notifyPropertyChanged(BR.teamList);
    }

    @Bindable
    public int getProgressBarListVisibility() {
        return mProgressBarListVisibility;
    }

    public void setProgressBarListVisibility(int progressBarListVisibility) {
        this.mProgressBarListVisibility = progressBarListVisibility;
         notifyPropertyChanged(BR.progressBarListVisibility);
    }

    /**
     * Get all the team data from Applaudo web service
     */
    public void getTeamsData(){
        /*Here we show the progress bar in the list*/
        if(mCurrentPage!=1){
            setProgressBarListVisibility(View.VISIBLE);
        }

        ApiManager.getInstance().getApplaudoService().getTeams().enqueue(new Callback<List<Team>>() {
            @Override
            public void onResponse(Call<List<Team>> call, Response<List<Team>> response) {
                setProgressBarVisibility(View.GONE);
                switch (response.code()){
                    case 200:
                     /*Here we simulate pagination for challenge requirement*/
                        if(mCurrentPage==1){
                            mTeamList.addAll(response.body().subList(0,10));
                            mTeamsAdapter.notifyDataSetChanged();
                            setListVisibility(View.VISIBLE);
                            setProgressBarListVisibility(View.GONE);
                        }else{
                            mTeamList.addAll(response.body().subList(10,20));
                            mTeamsAdapter.notifyDataSetChanged();
                            setListVisibility(View.VISIBLE);
                            setProgressBarListVisibility(View.GONE);
                        }
                        break;
                    default:
                        setListVisibility(View.GONE);
                        setProgressBarListVisibility(View.GONE);
                        setErrorMessage(mContext.getString(R.string.oops_something_was_wrong));
                        break;
                }
            }

            @Override
            public void onFailure(Call<List<Team>> call, Throwable t) {
                setProgressBarVisibility(View.GONE);
                setListVisibility(View.GONE);
                setProgressBarListVisibility(View.GONE);
                setErrorMessage(t.getMessage());
                setErrorMessageVisibility(View.VISIBLE);
            }
        });
    }

    /**
     * Try to make the las API call again if
     * an error happened
     */
    public void retryApiRequest(){
        setProgressBarVisibility(View.VISIBLE);
        setErrorMessage("");
        setErrorMessageVisibility(View.GONE);
        getTeamsData();
    }

    @BindingAdapter("teamsList")
    public static void bindTeamList(RecyclerView recyclerView, List<Team> teams) {
        mTeamList = teams;
        mTeamsAdapter= new TeamsAdapter(mTeamList,mTeamListener);
        LinearLayoutManager linearLayoutManager =
                new LinearLayoutManager(recyclerView.getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setOnScrollListener(new EndlessOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int currentPage) {
                if(currentPage<=2){
                    mCurrentPage=currentPage;
                    mOnListBottomReachListener.onListBottomReach();
                }
            }
        });
        recyclerView.setAdapter(mTeamsAdapter);
    }
}
