/*
 * Copyright 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.applaudostudios.codechallenge.android.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.media.MediaPlayer;
import android.net.Uri;
import android.view.View;
import android.widget.MediaController;
import android.widget.VideoView;

import com.applaudostudios.codechallenge.android.BR;
import com.applaudostudios.codechallenge.android.model.Team;

/**
 * Handle all the data between TeamDetailFragment class
 * and fragment_team_detail layout in order to update
 * all the data in the view.
 */
public class TeamDetailViewModel extends BaseObservable{
    private int mMessageTextVisibility= View.VISIBLE;
    private int mVideoProgressBarVisibility= View.VISIBLE;
    private int mVideoMessageVisibility= View.INVISIBLE;
    private int mMainContentVisibility= View.INVISIBLE;
    private String mImageUrl="";
    private String mTeamName="";
    private String mTeamDescription="";
    private String mVideoUrl="";
    private static OnVideoEventListener mOnVideoEventListener;
    private static VideoView mVideoView;
    private static int mVideoProgress=0;

    /**
     * Interface to notify th video player event
     */
    public  interface OnVideoEventListener{
        void onFailure();
        void onPrepared();
        void onShowProgress();
    }

    public TeamDetailViewModel(OnVideoEventListener listener){
        this.mOnVideoEventListener= listener;
    }

    @Bindable
    public int getMessageTextVisibility() {
        return mMessageTextVisibility;
    }

    public void setMessageTextVisiblity(int messageTextVisibility) {
        this.mMessageTextVisibility = messageTextVisibility;
        notifyPropertyChanged(BR.messageTextVisibility);
    }

    @Bindable
    public  int getVideoProgressBarVisibility() {
        return mVideoProgressBarVisibility;
    }


    public void setVideoProgressBarVisibility(int videoProgressBarVisibility) {
        this.mVideoProgressBarVisibility = videoProgressBarVisibility;
        notifyPropertyChanged(BR.videoProgressBarVisibility);
    }

    @Bindable
    public int getVideoMessageVisibility() {
        return mVideoMessageVisibility;
    }

    public void setVideoMessageVisibility(int videoMessageVisibility) {
        this.mVideoMessageVisibility = videoMessageVisibility;
        notifyPropertyChanged(BR.videoMessageVisibility);
    }

    @Bindable
    public int getMainContentVisibility() {
        return mMainContentVisibility;
    }

    public void setMainContentVisibility(int mainContentVisibility) {
        this.mMainContentVisibility = mainContentVisibility;
        notifyPropertyChanged(BR.mainContentVisibility);
    }

    @Bindable
    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.mImageUrl = imageUrl;
        notifyPropertyChanged(BR.imageUrl);
    }

    @Bindable
    public String getTeamName() {
        return mTeamName;
    }

    public void setTeamName(String teamName) {
        this.mTeamName = teamName;
        notifyPropertyChanged(BR.teamName);
    }

    @Bindable
    public String getTeamDescription() {
        return mTeamDescription;
    }

    public void setTeamDescription(String teamDescription) {
        this.mTeamDescription = teamDescription;
        notifyPropertyChanged(BR.teamDescription);
    }

    @Bindable
    public String getVideoUrl() {
        return mVideoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.mVideoUrl = videoUrl;
        notifyPropertyChanged(BR.videoUrl);
    }

    /**
     * Update all the content in the fragment_detail_layout
     * @param team the selected team to show
     */
    public void updateTeamData(Team team){
        setImageUrl(team.getImgLogo());
        setTeamName(team.getTeamName());
        setTeamDescription(team.getDescription());
        setVideoUrl(team.getVideoUrl());
        setMainContentVisibility(View.VISIBLE);
        setMessageTextVisiblity(View.INVISIBLE);
    }

    /**
     * This method help us to set up the video url
     * in order to show the video
     */
    @BindingAdapter("videoUrl")
    public static void setVideoUrlPath(VideoView videoView, String videoUrl) {
        if(videoUrl.compareTo("")!=0){

            mOnVideoEventListener.onShowProgress();
            mVideoView=videoView;
            mVideoView.setVisibility(View.INVISIBLE);
            mVideoView.setVisibility(View.VISIBLE);
            /*Here we hide and show the videoView to clea previous content*/
            MediaController videoController = new MediaController(videoView.getContext());
            videoController.setAnchorView(mVideoView);
            mVideoView.setMediaController(videoController);
            mVideoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                    mOnVideoEventListener.onFailure();
                    return true;
                }
            });
            mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    mOnVideoEventListener.onPrepared();
                    mVideoView.seekTo(mVideoProgress);
                }
            });
            mVideoView.setVideoURI(Uri.parse(videoUrl));
            mVideoView.start();
        }
    }

    /**
     * return the current position in the
     * video progress
     */
    public  int getCurrentVideoProgress(){
        int progress=0;
        if(mVideoView!=null){
            progress=mVideoView.getCurrentPosition();
        }
        return progress;
    }

    /**
     * Update the video progress for
     * previous play
     */
    public void updateVideoProgress(int videoProgress){
        mVideoProgress=videoProgress;
    }
}
