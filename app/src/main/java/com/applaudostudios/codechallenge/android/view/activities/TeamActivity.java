/*
 * Copyright 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.applaudostudios.codechallenge.android.view.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.applaudostudios.codechallenge.android.R;
import com.applaudostudios.codechallenge.android.model.Team;
import com.applaudostudios.codechallenge.android.view.fragments.TeamDetailFragment;
import com.applaudostudios.codechallenge.android.view.fragments.TeamFragment;

/**
 * Show the team list fragment in order to display
 * all the teams information.
 */
public class TeamActivity extends AppCompatActivity implements TeamFragment.OnTeamSelectionListener {

    /**
     * Create a single intent in order to start the
     * Team activity and show the team list.
     * @param context application´s context who call the activity
     */
    public static Intent getIntent(Context context){
        return new Intent(context,TeamActivity.class);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);

        //Here we force landscape orientation in tablet
       if(getResources().getBoolean(R.bool.is_tablet)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }

        //Here we initialize the toolbar
        initToolBar();
    }

    @Override
    public void onTeamSelected(Team team) {

         /*check if we are in portrait*/
        if(getResources().getConfiguration().orientation ==
                Configuration.ORIENTATION_PORTRAIT){
            startActivity(TeamDetailActivity.getIntent(this,team));
        }else{
            TeamDetailFragment teamDetailFragment = (TeamDetailFragment)getSupportFragmentManager()
                    .findFragmentById(R.id.fragment_team_detail);

            if(teamDetailFragment!=null){
                 /*update the detail fragment information*/
                teamDetailFragment.updateTeamData(team);
            }
        }
    }

    /**
     * Set the icon and the title in the tool bar
     */
    public void initToolBar(){
        getSupportActionBar().setIcon(R.drawable.ic_menu_home);
        getSupportActionBar().setTitle(R.string.applaudo_homework);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }
}
