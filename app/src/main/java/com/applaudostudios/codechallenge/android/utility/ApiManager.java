/*
 * Copyright 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.applaudostudios.codechallenge.android.utility;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Make all the API request in order to get all
 * the data using all the retrofit interface.
 */
public class ApiManager {
    private static final String URL_BASE="http://applaudostudios.com/external/";
    private static  ApiManager mApiManager;
    private ApplaudoService    mApplaudoService;
    private Retrofit           mRetrofit;

    /**
     * Create a single API manager instance
     */
    public ApiManager(){
        setUpRetrofit();
    }

    /**
     * Return an ApiManager instance creating a new one
     * if mApiManager is null.
     */
    public static ApiManager getInstance(){
        if(mApiManager == null){
            mApiManager= new ApiManager();
        }
        return mApiManager;
    }

    /**
     * Create a retrofit setting in order to
     * set up the base URL for our API and the
     * GSON converter
     */
    private  void setUpRetrofit(){
        mRetrofit = new Retrofit.Builder()
                .baseUrl(URL_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mApplaudoService = mRetrofit.create(ApplaudoService.class);
    }

    /**
     * Return an ApplaudoService class instance to
     * get all the interface to get make API requests.
     */
    public ApplaudoService getApplaudoService(){
        return mApplaudoService;
    }

}
