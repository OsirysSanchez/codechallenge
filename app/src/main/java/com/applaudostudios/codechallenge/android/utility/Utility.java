/*
 * Copyright 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.applaudostudios.codechallenge.android.utility;

import android.databinding.BindingAdapter;
import android.util.Log;
import android.widget.ImageView;

import com.applaudostudios.codechallenge.android.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

/**
 * This class contain a lot of methods to use
 * in all the application, they are use common
 */

public class Utility {
    /**
     * This method help us to handle the changes in the
     * view visibility with a cross fade animation.
     */
    @BindingAdapter("urlImage")
    public static void setImageUrl(ImageView imageView, String url) {
        if(url.compareTo("")!=0){
            Glide.with(imageView.getContext())
                    .load(url)
                    .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                            .error(R.drawable.no_img_available)
                            .placeholder(R.drawable.place_holder))
                    .into(imageView);
        }
    }
}
