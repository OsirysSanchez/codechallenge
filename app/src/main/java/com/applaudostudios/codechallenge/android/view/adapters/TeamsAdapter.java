/*
 * Copyright 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.applaudostudios.codechallenge.android.view.adapters;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.applaudostudios.codechallenge.android.R;
import com.applaudostudios.codechallenge.android.databinding.ItemTeamListBinding;
import com.applaudostudios.codechallenge.android.model.Team;
import com.applaudostudios.codechallenge.android.view.fragments.TeamFragment;

import java.util.List;

/**
 * Show a list of team elements in a recycler view widget
 */
public class TeamsAdapter extends RecyclerView.Adapter<TeamsAdapter.TeamViewHolder>{
    private List<Team> mTeamList;
    private TeamFragment.OnTeamSelectionListener mTeamListener;

    /**
     * Create a TeamsAdapter instance that help us
     * to show all the teams in recyclerview widget.
     * @param teamList all the team´s object to show in the list
     */
    public TeamsAdapter(List<Team> teamList,TeamFragment.OnTeamSelectionListener teamListener){
        mTeamList= teamList;
        mTeamListener=teamListener;
    }


    @Override
    public TeamViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       /*Here we get the row view*/
        View teamView = LayoutInflater.from( parent.getContext()).inflate(R.layout.item_team_list, parent, false);
        return new TeamViewHolder(teamView);
    }

    @Override
    public void onBindViewHolder(TeamViewHolder holder, int position) {
        holder.bind(mTeamList.get(position));
    }

    @Override
    public int getItemCount() {
        return mTeamList.size();
    }

    /**
     * Encapsulate all the data elements to show in a single
     * lit item handle the team data.
     */
    public class TeamViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private ItemTeamListBinding mItemTeamListBinding;

        public TeamViewHolder(View itemView) {
            super(itemView);
            mItemTeamListBinding = DataBindingUtil.bind(itemView);
            itemView.setOnClickListener(this);
        }

        /**
         * This method help us to set the team object instance
         * @param team the team object with the current data in the
         *             list element
         */
        public void bind(Team team){
            mItemTeamListBinding.setTeam(team);
        }

        @Override
        public void onClick(View v) {
            mTeamListener.onTeamSelected(mTeamList.get(getAdapterPosition()));
        }
    }


}
