/*
 * Copyright 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.applaudostudios.codechallenge.android.view.fragments;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.ShareActionProvider;

import com.applaudostudios.codechallenge.android.R;
import com.applaudostudios.codechallenge.android.databinding.FragmentTeamDetailBinding;
import com.applaudostudios.codechallenge.android.model.Team;
import com.applaudostudios.codechallenge.android.viewmodel.TeamDetailViewModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * This class show the team detail information
 * like video and location in a map
 */
public class TeamDetailFragment extends Fragment implements OnMapReadyCallback,TeamDetailViewModel.OnVideoEventListener {
    public static final String ARG_VIDEO_PROGRESS="video_progress";
    private FragmentTeamDetailBinding mFragmentTeamDetailBinding;
    private GoogleMap mMap;
    private TeamDetailViewModel mTeamDetailViewModel;
    private Team mTeam;
    private ShareActionProvider mShareActionProvider;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentTeamDetailBinding = DataBindingUtil.inflate(inflater,R.layout.fragment_team_detail,container,false);
        mTeamDetailViewModel = new TeamDetailViewModel(this);
        mFragmentTeamDetailBinding.setTeamDetailViewModel(mTeamDetailViewModel);
        return mFragmentTeamDetailBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        /*Check for previous video progress*/
        if(savedInstanceState!=null){
           mTeamDetailViewModel.updateVideoProgress(savedInstanceState.getInt(ARG_VIDEO_PROGRESS,0));
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        /*Here we save the current video progress*/
        outState.putInt(ARG_VIDEO_PROGRESS,mTeamDetailViewModel.getCurrentVideoProgress());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.team_detail_menu, menu);

        // Locate MenuItem with ShareActionProvider
        MenuItem item = menu.findItem(R.id.share);

        // Fetch and store ShareActionProvider
        mShareActionProvider= (ShareActionProvider) MenuItemCompat.getActionProvider(item);

        setShareIntent();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        updateLocationTeam();
    }

    /**
     * Update the selected team data in the fragment
     */
    public void updateTeamData(Team team){
        mTeam = team;
        mTeamDetailViewModel.updateTeamData(team);
        updateLocationTeam();
        /*Here prepare the share intent*/
        setShareIntent();
    }

    /**
     * Update the location for the current team
     */
    public void updateLocationTeam(){
        if(mTeam!=null && mMap!=null){
            //Here we clear the map
            mMap.clear();
            // Add a marker in a new location and move the camera
            LatLng location = new LatLng(Double.valueOf(mTeam.getLatitude()), Double.valueOf(mTeam.getLongitude()));
            mMap.addMarker(new MarkerOptions().position(location).title(mTeam.getTeamName()));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(location));
        }
    }

    @Override
    public void onFailure() {
        mTeamDetailViewModel.setVideoProgressBarVisibility(View.GONE);
        mTeamDetailViewModel.setVideoMessageVisibility(View.VISIBLE);
    }

    @Override
    public void onPrepared() {
        mTeamDetailViewModel.setVideoProgressBarVisibility(View.GONE);
        mTeamDetailViewModel.setVideoMessageVisibility(View.GONE);
    }

    @Override
    public void onShowProgress() {
        mTeamDetailViewModel.setVideoProgressBarVisibility(View.VISIBLE);
    }

    /**
     * Call to update the share intent
     */
    private void setShareIntent() {
        if (mShareActionProvider != null && mTeam!=null) {
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, mTeam.getTeamName());
            shareIntent.putExtra(Intent.EXTRA_TEXT, mTeam.getDescription());
            shareIntent.setType("text/plain");
            mShareActionProvider.setShareIntent(shareIntent);
        }
    }
}
