/*
 * Copyright 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.applaudostudios.codechallenge.android.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Handle all the team data like name,
 * address, logo url, location, coach, etc.
 */
public class Team implements Parcelable {
    @SerializedName("id")
    private int mId;

    @SerializedName("team_name")
    private String mTeamName;

    @SerializedName("since")
    private String mSince;

    @SerializedName("coach")
    private String mCoach;

    @SerializedName("team_nickname")
    private String mTeamNickname;

    @SerializedName("stadium")
    private String mStadium;

    @SerializedName("img_logo")
    private String mImgLogo;

    @SerializedName("img_stadium")
    private String mImgStadium;

    @SerializedName("latitude")
    private String mLatitude;

    @SerializedName("longitude")
    private String mLongitude;

    @SerializedName("website")
    private String mWebsite;

    @SerializedName("tickets_url")
    private String mTicketsUrl;

    @SerializedName("address")
    private String mAddress;

    @SerializedName("phone_number")
    private String mPhoneNumber;

    @SerializedName("description")
    private String mDescription;

    @SerializedName("video_url")
    private String mVideoUrl;

    @SerializedName("schedule_games")
    private List<ScheduleGame> mScheduleGames;

    protected Team(Parcel in) {
        mId = in.readInt();
        mTeamName = in.readString();
        mSince = in.readString();
        mCoach = in.readString();
        mTeamNickname = in.readString();
        mStadium = in.readString();
        mImgLogo = in.readString();
        mImgStadium = in.readString();
        mLatitude = in.readString();
        mLongitude = in.readString();
        mWebsite = in.readString();
        mTicketsUrl = in.readString();
        mAddress = in.readString();
        mPhoneNumber = in.readString();
        mDescription = in.readString();
        mVideoUrl = in.readString();
    }

    public static final Creator<Team> CREATOR = new Creator<Team>() {
        @Override
        public Team createFromParcel(Parcel in) {
            return new Team(in);
        }

        @Override
        public Team[] newArray(int size) {
            return new Team[size];
        }
    };

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public String getTeamName() {
        return mTeamName;
    }

    public void setTeamName(String teamName) {
        this.mTeamName = teamName;
    }

    public String getSince() {
        return mSince;
    }

    public void setSince(String since) {
        this.mSince = since;
    }

    public String getCoach() {
        return mCoach;
    }

    public void setCoach(String coach) {
        this.mCoach = coach;
    }

    public String getTeamNickname() {
        return mTeamNickname;
    }

    public void setTeamNickname(String teamNickname) {
        this.mTeamNickname = teamNickname;
    }

    public String getStadium() {
        return mStadium;
    }

    public void setStadium(String stadium) {
        this.mStadium = stadium;
    }

    public String getImgLogo() {
        return mImgLogo;
    }

    public void setImgLogo(String imgLogo) {
        this.mImgLogo = imgLogo;
    }

    public String getImgStadium() {
        return mImgStadium;
    }

    public void setImgStadium(String imgStadium) {
        this.mImgStadium = imgStadium;
    }

    public String getLatitude() {
        return mLatitude;
    }

    public void setLatitude(String latitude) {
        this.mLatitude = latitude;
    }

    public String getLongitude() {
        return mLongitude;
    }

    public void setLongitude(String longitude) {
        this.mLongitude = longitude;
    }

    public String getWebsite() {
        return mWebsite;
    }

    public void setWebsite(String website) {
        this.mWebsite = website;
    }

    public String getTicketsUrl() {
        return mTicketsUrl;
    }

    public void setTicketsUrl(String ticketsUrl) {
        this.mTicketsUrl = ticketsUrl;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        this.mAddress = address;
    }

    public String getPhoneNumber() {
        return mPhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.mPhoneNumber = phoneNumber;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        this.mDescription = description;
    }

    public String getVideoUrl() {
        return mVideoUrl;
    }

    public void setmVideoUrl(String videoUrl) {
        this.mVideoUrl = videoUrl;
    }

    public List<ScheduleGame> getScheduleGames() {
        return mScheduleGames;
    }

    public void setScheduleGames(List<ScheduleGame> scheduleGames) {
        this.mScheduleGames = scheduleGames;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mId);
        dest.writeString(mTeamName);
        dest.writeString(mSince);
        dest.writeString(mCoach);
        dest.writeString(mTeamNickname);
        dest.writeString(mStadium);
        dest.writeString(mImgLogo);
        dest.writeString(mImgStadium);
        dest.writeString(mLatitude);
        dest.writeString(mLongitude);
        dest.writeString(mWebsite);
        dest.writeString(mTicketsUrl);
        dest.writeString(mAddress);
        dest.writeString(mPhoneNumber);
        dest.writeString(mDescription);
        dest.writeString(mVideoUrl);
    }
}

