/*
 * Copyright 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.applaudostudios.codechallenge.android.utility;
import com.applaudostudios.codechallenge.android.model.Team;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Encapsulate all the interfaces in order to make
 * all the API call and get all the data from the endpoints.
 */
public interface ApplaudoService {

    @GET("applaudo_homework.json")
    Call<List<Team>> getTeams();
}
