/*
 * Copyright 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.applaudostudios.codechallenge.android.view.fragments;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.applaudostudios.codechallenge.android.R;
import com.applaudostudios.codechallenge.android.databinding.FragmentTeamBinding;
import com.applaudostudios.codechallenge.android.model.Team;
import com.applaudostudios.codechallenge.android.viewmodel.TeamViewModel;

/**
 * Show a team list with all team information like
 * name, address and logo.
 */
public class TeamFragment extends Fragment implements TeamViewModel.OnListBottomReachListener{
    private FragmentTeamBinding mFragmentTeamBinding;
    private TeamViewModel       mTeamViewModel;
    private OnTeamSelectionListener mTeamListener;

    /**
     * Receive the team selected by the user in the list
     */
    public interface OnTeamSelectionListener{
        void onTeamSelected(Team team);
    }

    /**
     * Return a single TeamFragment instance
     * in order to load the team list.
     */
    public static TeamFragment getInstance(){
        return new TeamFragment();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mTeamListener = (OnTeamSelectionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement OnTeamSelectedListener");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(false);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentTeamBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_team,container,false);
        mTeamViewModel= new TeamViewModel(getContext(),mTeamListener,this);
        mFragmentTeamBinding.setTeamViewModel(mTeamViewModel);
        return mFragmentTeamBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTeamViewModel.getTeamsData();
    }

    @Override
    public void onListBottomReach() {
        mTeamViewModel.getTeamsData();
    }

}
